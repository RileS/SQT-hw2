/**
 * BLG475E - Software Quality & Testing
 * Homework 2 - Unit Testing
 *
 ***   written by
 ***   Rumeysa Bulut - 150130003
 ***   Safa Keskin   - 150140137
 ***   19.11.2017
 */

package com.aric.samples.account.model;

import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class AccountTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testGetAndSetId() throws Exception {
        Account account = new Account();
        account.setId(2);
        Assert.assertEquals(2, account.getId());
    }

    @Test
    public void testGetAndSetBalance() throws Exception {
        Account account = new Account();
        Double doubleBalance = new Double(234.3);
        account.setBalance(doubleBalance);
        Assert.assertEquals(doubleBalance, new Double(account.getBalance()));
    }

    @Test
    public void testGetAndSetOwnerTckn() throws Exception {
        Account account = new Account();
        account.setOwnerTckn(123456);
        Assert.assertEquals(123456, account.getOwnerTckn());
    }

    @Test
    public void testGetAndSetOwnerFirstName() throws Exception {
        Account account = new Account();
        account.setOwnerFirstName("Safa");
        Assert.assertEquals("Safa", account.getOwnerFirstName());
    }

    @Test
    public void testGetAndSetOwnerLastName() throws Exception {
        Account account = new Account();
        account.setOwnerLastName("Keskin");
        Assert.assertEquals("Keskin", account.getOwnerLastName());
    }

    @Test
    public void testDeposit() throws Exception {
        Account account = new Account();
        account.setBalance(23.2);
        Assert.assertEquals( new Double(23.2 + 13.2), new Double(account.deposit(13.2)) );
    }

    @Test
    public void testWithdraw() throws Exception {
        Account account = new Account();
        account.setBalance(20);
        Assert.assertEquals(new Double(20 - 2.4), new Double(account.withdraw(2.4)));
    }

    @Test
    public void testDepositWithNegativeAmount() throws Exception{
        try {
            Account account = new Account();
            account.deposit(-29);
        }catch (IllegalArgumentException e){
            System.out.println(e);
        }
    }

    @Test
    public void testWithdrawMoreThanBalance() throws Exception {
        try {
            Account account = new Account();
            account.setBalance(20);
            account.withdraw(30);
        }
        catch (IllegalArgumentException e){
            System.out.println(e);
        }
    }

}