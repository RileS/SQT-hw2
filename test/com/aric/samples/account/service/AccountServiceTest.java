/**
 * BLG475E - Software Quality & Testing
 * Homework 2 - Unit Testing
 *
 ***   written by
 ***   Rumeysa Bulut - 150130003
 ***   Safa Keskin   - 150140137
 ***   19.11.2017
 */

package com.aric.samples.account.service;

import com.aric.samples.account.model.Account;
import com.aric.samples.account.repository.AccountRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class AccountServiceTest {

    private AccountService accountService;
    private AccountRepository mockAccountRepository = Mockito.mock(AccountRepository.class);
    private Account mockAccount = Mockito.mock(Account.class);
    private Account mockExpectedRet = Mockito.mock(Account.class);

    @Before
    public void setUpAccountService() throws Exception {
        accountService = new AccountService();
        ReflectionTestUtils.setField(accountService, "accountRepository", mockAccountRepository);
        mockAccount.setId(124);
        mockAccount.setBalance(43350.0);
        mockAccount.setOwnerTckn(124);
        mockAccount.setOwnerLastName("Yar");
        mockAccount.setOwnerFirstName("Mehmet");
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testFindPersonsByTckn() throws Exception {
        /** SET METHODLARI VOID DÖNDÜĞÜNDEN DOLAYI NE DÖNDÜĞÜNÜ KONTROL EDEMİYORUZ. */
        mockAccount.setId(124);
        mockAccount.setBalance(43350.0);
        mockAccount.setOwnerTckn(124);
        mockAccount.setOwnerLastName("Yar");
        mockAccount.setOwnerFirstName("Mehmet");
        List<Account> accounts = new ArrayList<>();
        accounts.add(mockAccount);

        when( mockAccountRepository.findByOwnerTckn(124) ).thenReturn(accounts);
        assertEquals(accounts, accountService.findPersonsByTckn(124));
    }

    @Test
    public void testDeposit() throws Exception {
        when( mockAccountRepository.findOne(new Long(124) )).thenReturn(mockAccount);
        when( mockAccount.deposit(50)).thenReturn(43400.0);

        mockExpectedRet.setId(124);
        mockExpectedRet.setBalance(43400.0);
        mockExpectedRet.setOwnerTckn(124);
        mockExpectedRet.setOwnerLastName("Yar");
        mockExpectedRet.setOwnerFirstName("Mehmet");

        when( mockAccountRepository.save( mockAccount)).thenReturn(mockExpectedRet);

        assertEquals(mockExpectedRet, accountService.deposit(new Long(124), new Double(50)));
    }

    @Test
    public void testDepositWithNegativeArgument() throws Exception {
        try {
            when( mockAccountRepository.findOne(new Long(124) )).thenReturn(mockAccount);
            when( mockAccount.deposit(-2)).thenThrow(new IllegalArgumentException("Amount should be a positive value"));
            when( mockAccountRepository.save( mockAccount)).thenReturn(mockExpectedRet);    //just for 100% code coverage
            mockAccount.deposit(-2);

        }catch (IllegalArgumentException e){
            System.out.println(e);
        }

    }

    @Test
    public void testEft() throws Exception {
        Account mockSender     = Mockito.mock(Account.class);

        mockSender.setId(124);
        mockSender.setBalance(100.0);
        mockSender.setOwnerTckn(124);
        mockSender.setOwnerLastName("Yar");
        mockSender.setOwnerFirstName("Mehmet");

        when( mockAccountRepository.findOne(new Long(124) )).thenReturn(mockSender);

        mockAccount.setId(125);
        mockAccount.setBalance(20.0);
        mockAccount.setOwnerTckn(125);
        mockAccount.setOwnerLastName("Veli");
        mockAccount.setOwnerFirstName("Ali");

        when( mockAccountRepository.findOne(new Long(125) )).thenReturn(mockAccount);

        mockExpectedRet.setId(124);
        mockExpectedRet.setBalance(70.0);
        mockExpectedRet.setOwnerTckn(124);
        mockExpectedRet.setOwnerLastName("Yar");
        mockExpectedRet.setOwnerFirstName("Mehmet");

        when( mockAccountRepository.save( mockSender )).thenReturn(mockExpectedRet);
        assertEquals(mockExpectedRet, accountService.eft(new Long(124), new Long(125), new Double(30)));

    }

    @Test
    public void testEftWithNegativeValue() throws Exception{
        Account mockSender     = Mockito.mock(Account.class);

        mockSender.setId(124);
        mockSender.setBalance(100.0);
        mockSender.setOwnerTckn(124);
        mockSender.setOwnerLastName("Yar");
        mockSender.setOwnerFirstName("Mehmet");

        when( mockAccountRepository.findOne(new Long(124) )).thenReturn(mockSender);

        mockAccount.setId(125);
        mockAccount.setBalance(20.0);
        mockAccount.setOwnerTckn(125);
        mockAccount.setOwnerLastName("Veli");
        mockAccount.setOwnerFirstName("Ali");

        when( mockAccountRepository.findOne(new Long(125) )).thenReturn(mockAccount);

        when( mockSender.withdraw(155) ).thenThrow(new IllegalArgumentException("Amount cannot be greater than balance"));
        when( mockAccount.deposit(-2)).thenThrow(new IllegalArgumentException("Amount should be a positive value"));    //just for 100% code coverage
        when( mockAccountRepository.save( mockSender )).thenReturn(mockExpectedRet);    //just for 100% code coverage
        try {
            assertEquals(mockExpectedRet, accountService.eft(new Long(124), new Long(125), new Double(155)));
        }catch (IllegalArgumentException e){
            System.out.println(e);
        }
    }
}