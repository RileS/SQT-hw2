/**
 * BLG475E - Software Quality & Testing
 * Homework 2 - Unit Testing
 *
 ***   written by
 ***   Rumeysa Bulut - 150130003
 ***   Safa Keskin   - 150140137
 ***   19.11.2017
 */

package com.aric.samples.account.controller;

import com.aric.samples.account.model.Account;
import com.aric.samples.account.service.AccountService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class AccountControllerTest {
    private AccountController accountController;
    private AccountService mockService = Mockito.mock(AccountService.class);
    private Account mockAccount = Mockito.mock(Account.class);

    @Before
    public void setUp() throws Exception {
        accountController = new AccountController();
        ReflectionTestUtils.setField(accountController, "service", mockService);
    }

    @After
    public void tearDown() throws Exception { }

    @Test
    public void testAccount() throws Exception {
        mockAccount.setId(124);
        mockAccount.setBalance(10.0);
        mockAccount.setOwnerTckn(124);
        mockAccount.setOwnerLastName("Yar");
        mockAccount.setOwnerFirstName("Mehmet");
        List<Account> accounts = new ArrayList<>();
        accounts.add(mockAccount);

        when(mockService.findPersonsByTckn(124)).thenReturn(accounts);
        assertEquals(accounts,accountController.account(124));
    }

    @Test
    public void testDeposit() throws Exception {
        mockAccount.setId(124);
        mockAccount.setBalance(40.0);
        mockAccount.setOwnerTckn(124);
        mockAccount.setOwnerLastName("Yar");
        mockAccount.setOwnerFirstName("Mehmet");

        when(mockService.deposit(124L, 30.0)).thenReturn(mockAccount);
        assertEquals(mockAccount, accountController.deposit(124L, 30.0));
    }

    @Test
    public void testDepositNoMoney() throws Exception{
        mockAccount.setId(124);
        mockAccount.setBalance(40.0);
        mockAccount.setOwnerTckn(124);
        mockAccount.setOwnerLastName("Yar");
        mockAccount.setOwnerFirstName("Mehmet");

        when(mockService.deposit(124L, 0.0)).thenReturn(mockAccount);
        assertEquals(mockAccount, accountController.deposit(124L, 0.0));
    }

    @Test
    public void testWithdraw() throws Exception {
        mockAccount.setId(124);
        mockAccount.setBalance(40.0);
        mockAccount.setOwnerTckn(124);
        mockAccount.setOwnerLastName("Yar");
        mockAccount.setOwnerFirstName("Mehmet");

        Account expectedSender = Mockito.mock(Account.class);
        expectedSender.setId(124);
        expectedSender.setBalance(10.0);
        expectedSender.setOwnerTckn(124);
        expectedSender.setOwnerLastName("Yar");
        expectedSender.setOwnerFirstName("Mehmet");

        when(mockService.eft(124L, 125L , 30.0)).thenReturn(expectedSender);
        assertEquals(expectedSender, accountController.withdraw(124L, 125L , 30.0) );
    }

}